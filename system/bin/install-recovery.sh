#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:24350030:5633e00e99c4bb78f3884056ae55741757678721; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:21878090:e7b5e025b37edd2e34848ba7394ad9977f86e17e EMMC:/dev/block/bootdevice/by-name/recovery 5633e00e99c4bb78f3884056ae55741757678721 24350030 e7b5e025b37edd2e34848ba7394ad9977f86e17e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
