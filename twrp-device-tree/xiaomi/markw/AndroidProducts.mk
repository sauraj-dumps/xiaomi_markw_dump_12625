#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_markw.mk

COMMON_LUNCH_CHOICES := \
    omni_markw-user \
    omni_markw-userdebug \
    omni_markw-eng
